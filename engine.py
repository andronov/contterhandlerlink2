from tornado import gen


class ParserSite(object):

    def __init__(self, link_id, site_id, connection):
        self.link_id = link_id
        self.site_id = site_id
        self.connection = connection

    def run(self):
        self.create_user_link()

    @gen.coroutine
    def create_user_link(self):
        """
        ДОбавляем к юзеру ссылку
        :param base_link:
        :return:
        """
        print('creayr', self.link_id, self.site_id)
        cursor = yield self.connection.execute("SELECT id, user_id, wall_id "
                                               "FROM user_wall_settings WHERE is_active = True "
                                               "AND sources @> '{\"sites\": [%s]}' AND ("
                                               "words != '{\"excluding\": [], \"matching\": []}' OR "
                                               "words != '{\"excluding\": [\"\"], \"matching\": [\"\"]}');" %
                                               str(self.link_id))
        for wall in cursor.fetchall():
            print('wall', wall)
            cursor = yield self.connection.execute("SELECT * FROM user_wall_link WHERE user_id = {0} "
                                                   "AND wall_id = {1} AND link_id = {2} LIMIT 1;".format(
                                                   wall.user_id, wall.wall_id, self.link_id))
            if cursor.fetchone():
                return

            cursor = yield self.connection.execute("SELECT * FROM base_link WHERE id = {0};".format(self.link_id))
            link = cursor.fetchone()

            cursor = yield self.connection.execute(
                    "INSERT INTO user_wall_link (user_id, wall_id, link_id, is_active, created) "
                    "VALUES ({0},{1},{2},'{3}','{4}') RETURNING *;".format(wall.user_id, wall.wall_id, self.link_id, True,
                                                                            link.created))

            item = cursor.fetchone()
            print(item.id, item)

        return